package exceptions;

public class InvalidOperandException extends Exception
{
	private static final long serialVersionUID = -732168095714054707L;

	public InvalidOperandException()
	{
		super();
	}

	public InvalidOperandException(String message)
	{
		super(message);
	}
}