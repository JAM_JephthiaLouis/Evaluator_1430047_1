package exceptions;

public class MisplacedOperatorException extends Exception
{
	private static final long serialVersionUID = 4110078721518735269L;

	public MisplacedOperatorException()
	{
		super();
	}

	public MisplacedOperatorException(String message)
	{
		super(message);
	}
}